import java.util.concurrent.BlockingQueue;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addClient(BlockingQueue<Coada> cozi, Client c) {

        int min = 2000;
        for (Coada coada : cozi) {
            if (coada.getCoadaTime() < min) {
                min = coada.getCoadaTime();
            }
        }
        for (Coada coada :cozi) {
            if (coada.getCoadaTime() == min) {
                coada.addClient(c);
               // System.out.println("Cl "+c.toString()+" este la coada "+ coada.getNrCasa());
                View.getLog().append(c.toString()+ " la casa "+ coada.getNrCasa()+"\n");
                break;
            }
        }
    }

}