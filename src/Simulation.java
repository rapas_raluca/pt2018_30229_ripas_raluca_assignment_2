
import java.util.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import static java.lang.Thread.sleep;

public class Simulation implements Runnable{
    private int simTime;//=30;
    private int maxArr;//=10;
    private int minArr;//=2;
    private int minServ;//=1;
    private int maxServ;//=3;
    private int maxNrClientiSim;//=30;
    private int nrCozi;//=3;
    private static int nrClient=0;
    private static int currentTime=0;
    private SelectionPolicy policy;//= SelectionPolicy.SHORTEST_TIME;

    private Object lock = new Object();

    private int peakHour = 0;

    private SimulationFrame inter;

    private Schedular schedular;

    private View view;

    private List<Client> generatedClients;

    public Simulation(int simTime,int maxArr,int minArr, int minServ,int maxServ, int maxNrClientiSim,int nrCozi,View v,String strategy){
        if(strategy.equals("Shortest Time")){
            policy=SelectionPolicy.SHORTEST_TIME;
        }

        if(strategy.equals("Shortest Queue")){
            policy=SelectionPolicy.SHORTEST_QUEUE;
        }

        this.simTime=simTime;
        this.maxArr=maxArr;
        this.minArr=minArr;
        this.maxServ=maxServ;
        this.minServ=minServ;
        this.nrCozi=nrCozi;
        this.maxNrClientiSim=maxNrClientiSim;
        this.view=v;
    }

    private void generateClients(){
        for(int i=0;i<maxNrClientiSim;i++) {
            int servingTime = 0, arrivingTime = 0;
            Random random = new Random();
            servingTime = random.nextInt((maxServ - minServ) + 1) + minServ;
            arrivingTime = random.nextInt((maxArr - minArr) + 1) + minArr;
            Client client = new Client(arrivingTime, servingTime,nrClient);
            nrClient++;
            generatedClients.add(client);
        }

    }

    private int maxPeak=0;
    private void calculatePeakHour(int currentTime, BlockingQueue<Coada> cozi){
        int max=0;
        for(Coada c: cozi){
            if(c.getCoadaTime()>max)
                max=c.getCoadaTime();
        }
        if(maxPeak<max) {
            maxPeak = max;
            peakHour=currentTime;
        }
   }

    public static int getCurrentTime(){
        return currentTime;
    }

    private void init(){
        schedular= new Schedular(nrCozi,maxNrClientiSim);
        generatedClients=new ArrayList<Client>();
        schedular.changeStrategy(policy);
        generateClients();
        Collections.sort(generatedClients);
        for(Client c:generatedClients)
            System.out.println(c.toString());
        System.out.println("\n");
        inter=new SimulationFrame();
        inter.setCozi(schedular.getCozi());
        view.setPanel(inter);
    }

    private void avrTimeEmpty(){
        int i=1;
        for(Coada c: schedular.getCozi()){
            if(i>4)
                break;
            else
            {
                view.getLog().append("AVR empty queue for "+i+" queue is "+ (float)c.getNoClientsTime()/c.getNrClientiQueue()+"\n");
                i++;
            }
        }
    }

    private void avrServTime(){
        int i=1;
        for(Coada c: schedular.getCozi()){
            if(i>4)
                break;
            else
            {
                view.getLog().append("AVR service time for "+i+" queue is "+ (float)c.getServiceTime()/c.getNrClientiQueue()+"\n");
                i++;
            }
        }
    }

    private void avrWaitingTime(){
        int i=1;
        for(Coada c: schedular.getCozi()){
            if(i>4)
                break;
            else
            {
                view.getLog().append("AVR waiting time for "+i+" queue is "+ (float)c.getWaitingTime()/c.getNrClientiQueue()+"\n");
                i++;
            }
        }
    }

    private void setAvr(){
        avrTimeEmpty();
        view.getLog().append("\n");
        avrServTime();
        view.getLog().append("\n");
        avrWaitingTime();
        view.getLog().append("\n");
        view.getLog().append("PEAK HOUR IS AT : " + peakHour+"\n");
    }

    @Override
    public  void run(){
        init();
        while (currentTime<simTime){
            view.setTimeText(""+currentTime);
            Iterator<Client> i= generatedClients.iterator();
            while(i.hasNext()){
                synchronized (lock) {
                     Client c=i.next();
                    if (c.getArrivTime() == currentTime) {
                        schedular.dispatchClient(c);
                        lock.notifyAll();
                        i.remove();
                    }
               }
            }
            calculatePeakHour(currentTime,schedular.getCozi());
            currentTime++;
            synchronized (this){
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            inter.draw();
        }
        schedular.closeQueues();
        view.setTimeText(""+currentTime);
        setAvr();
    }
}
