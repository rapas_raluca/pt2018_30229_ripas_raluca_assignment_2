
import java.awt.Color;
import java.awt.Graphics;
import java.util.concurrent.BlockingQueue;
import javax.swing.JPanel;

public class SimulationFrame extends JPanel {

    private BlockingQueue<Coada> cozi;


  /*  public SimulationFrame(BlockingQueue<Coada> cozi) {
        this.cozi=cozi;
    }*/

    public SimulationFrame(){

    }

    public void draw() {
        this.repaint();
    }

    @Override
    public void paintComponent(Graphics graph) {
        super.paintComponent(graph);
        int x = 10,y;
        for (Coada c: cozi){
            y = 10;
            if(c.getNrClienti()==0) {
                drawEmptyQueue(graph, x, y);
            }else{
                drawQueue(graph,x,y);
            }
            y=13;
            for (int i = 0; i < c.getNrClienti(); i++) {
                y=y+30;
                if(i==0) {
                    drawClientAtQueue(graph,x,y);
                }else {
                    drawClient(graph, x, y);
                }
            }
            x=x+40;
        }
    }

    private void drawQueue(Graphics g, int x, int y) {    //deseneaza casa
        g.setColor(Color.decode("#CD5C5C"));
        g.fill3DRect(x,y,30,30,true);
        repaint();
    }


    private void drawClientAtQueue(Graphics g, int x, int y) {     // deseneaza clientul la casa
        g.setColor(Color.red);
        g.fillOval(x, y, 15, 15);
        repaint();
    }
    private void drawEmptyQueue(Graphics g, int x, int y) {        //deseneaza casa fara clienti
        g.setColor(Color.PINK);
        g.fill3DRect(x,y,30,30,true);
        repaint();
    }

    private void drawClient(Graphics g, int x, int y) {          // deseneaza clientul in asteptare
        g.setColor(Color.decode("#FA8072"));
        g.fillOval(x, y, 15, 15);
        repaint();
    }

    public void setCozi(BlockingQueue<Coada> cozi){
        this.cozi=cozi;
    }
}

