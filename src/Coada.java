

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada extends Thread {

    private int coadaTime=0;
    private int noClientsTime=0;
    private int maxNrCl=0;
    private  int nrCasa;
    private int nrClientiQueue=0;
    private int serviceTime=0;
    private int waitingTime=0;
    private final AtomicBoolean running = new AtomicBoolean(false);

    private Queue<Client> coada;

    public Coada(int maxNrCl, int nrCasa){
        this.maxNrCl=maxNrCl;
        this.coada=new LinkedList<Client>();
        this.nrCasa=nrCasa;

    }

    public void setCoadaTime(int coadaTime){
        this.coadaTime=coadaTime;
    }

    public int getCoadaTime(){
        return coadaTime;
    }

    public int getNrCasa(){
        return nrCasa;
    }

    public void addClient(Client c){
        coada.add(c);
        this.setCoadaTime(coadaTime+c.getServTime());
    }

    public void removeClient(){
        while (coada.size() == 0) {
        }
        Client c=coada.element();
        coada.remove();
        this.setCoadaTime(coadaTime-c.getServTime());
    }

    public int getNrClienti() {
        return coada.size();
    }

    public int getNoClientsTime(){
        return noClientsTime;
    }

    public int getServiceTime(){
        return serviceTime;
    }

    public int getWaitingTime(){
        return waitingTime;
    }

    public void stopThread(){
        running.set(false);
        //this.interrupt();
    }

    public void run(){
        running.set(true);
        while (running.get()) {
            if (!coada.isEmpty()) {
                int serv = 0;
                synchronized (this) {
                    try {
                        serv = coada.element().getServTime();
                        sleep(serv * 1000);
                        View.getLog().append(coada.element().toString2() + Simulation.getCurrentTime() + "<--\n");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    waitingTime = waitingTime + coadaTime;
                    serviceTime = serviceTime + serv;
                    this.removeClient();
                    nrClientiQueue++;
                }
            } else {
                synchronized (this) {
                    try {
                        noClientsTime++;
                        wait(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public int getNrClientiQueue(){
        return nrClientiQueue;
    }
    public void printClients(){
        for(Client c: coada){
            System.out.println("CLIENT " + c.toString());
        }
    }

}
