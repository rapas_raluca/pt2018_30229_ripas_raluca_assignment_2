
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Schedular {

    private BlockingQueue<Coada> cozi;

    private Strategy strategy;

    public Schedular(int maxNrCozi, int maxClientiCoada){
        int i;
            cozi = new ArrayBlockingQueue<Coada>(maxNrCozi);
            for (i = 0; i < maxNrCozi; i++) {
                Coada c = new Coada(maxClientiCoada, i + 1);
                cozi.add(c);
                c.start();
            }

    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy==SelectionPolicy.SHORTEST_QUEUE){
            strategy=new ConcreteStrategyQueue();
        }
        if(policy==SelectionPolicy.SHORTEST_TIME){
            strategy=new ConcreteStrategyTime();
        }
    }

    public  void dispatchClient(Client c){
        strategy.addClient(cozi,c);
    }

    public BlockingQueue<Coada> getCozi(){
        return cozi;
    }

    public void closeQueues(){
        for(Coada c:cozi){
            c.stopThread();
        }
    }

}
