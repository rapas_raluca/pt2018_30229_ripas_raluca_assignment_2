public class Client implements Comparable<Client>{


    private int nrClient;
    private int arrivTime;
    private int servTime;

    public Client(int arrivTime,int servTime,int nrClient){
        this.nrClient=nrClient;
        this.arrivTime=arrivTime;
        this.servTime=servTime;
    }

    public int getNrClient() {
        return nrClient;
    }

    public int getServTime() {
        return servTime;
    }

    public int getArrivTime() {
        return arrivTime;
    }

    @Override
    public int compareTo(Client c) {
        int compTime=((Client)c).getArrivTime();
        return this.arrivTime-compTime;
    }
    public String toString() {
        String rez;
        rez = "-->Clientul cu nr" + getNrClient() + " ajunge la " + getArrivTime() + " cu serv " + getServTime();
        return rez;
    }
    public String toString2() {
        String rez;
        rez = "<--Clientul cu numarul " + getNrClient() + " a ajuns la " + getArrivTime() + " si a plecat la ";
        return rez;
    }
}
