import java.util.List;
import java.util.concurrent.BlockingQueue;

public class ConcreteStrategyQueue implements Strategy {
    @Override
    public void addClient(BlockingQueue<Coada> cozi, Client c) {

            int min = 2000;
            for (Coada coada : cozi) {
                if (coada.getNrClienti() < min) {
                    min = coada.getNrClienti();
                }
            }
            for (Coada coada :cozi) {
                if (coada.getNrClienti() == min) {
                    coada.addClient(c);
                    View.getLog().append(c.toString()+ " la coada "+ coada.getNrCasa()+"\n");
                  //  System.out.println("Cl "+c.toString()+" este la coada "+ coada.getNrCasa());
                    break;
                }
            }

        }

}
