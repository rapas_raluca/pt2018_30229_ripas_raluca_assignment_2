import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

public class View extends JPanel {

        private  JFrame frame= new JFrame();
        private JPanel panel=new JPanel();
        private JTextField minArriving=new JTextField();
        private JTextField maxArriving=new JTextField("");
        private JTextField minService=new JTextField();
        private JTextField maxService=new JTextField();
        private JTextField nrClienti=new JTextField();
        private JTextField nrQueue=new JTextField();
        private JLabel maxA = new JLabel("Max Arriving:");
        private JLabel minA = new JLabel("Min Arriving:");
        private JLabel maxS = new JLabel("Max Service:");
        private JLabel minS = new JLabel("Min Service:");
        private JLabel nrC = new JLabel("Clients:");
        private JLabel nrQ = new JLabel("Queues:");
        private JLabel time = new JLabel("Time:");
        private JTextField timeText= new JTextField();
        private JComboBox<String> strategy= new JComboBox<String>();
        private JButton start= new JButton("Start");

        private static JTextArea log= new JTextArea();
        private JScrollPane scr= new JScrollPane(log);

        private JLabel simulationTime= new JLabel("Sim time:");
        private JTextField simTime= new JTextField();



        public View() {
            frame.setTitle("Queue");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(800, 600);
            frame.setLocation(350, 100);
            frame.getContentPane().setLayout(null);

            addMinMax();

            addOptions();

            addComponents();

            butonStart(this);
            frame.getContentPane().setBackground(Color.decode("#FAEBD7"));
            frame.setVisible(true);

        }

        private void addMinMax(){
            minA.setBounds(20,10,100,20);
            frame.getContentPane().add(minA);
            minArriving.setBounds(100,10,50,20);
            frame.getContentPane().add(minArriving);
            maxA.setBounds(200,10,100,20);
            frame.getContentPane().add(maxA);
            maxArriving.setBounds(280,10,50,20);
            frame.getContentPane().add(maxArriving);

            minS.setBounds(20,40,100,20);
            frame.getContentPane().add(minS);
            minService.setBounds(100,40,50,20);
            frame.getContentPane().add(minService);
            maxS.setBounds(200,40,100,20);
            frame.getContentPane().add(maxS);
            maxService.setBounds(280,40,50,20);
            frame.getContentPane().add(maxService);
        }

        private void addOptions(){
            nrC.setBounds(20,70,100,20);
            frame.getContentPane().add(nrC);
            nrClienti.setBounds(100,70,50,20);
            frame.getContentPane().add(nrClienti);
            nrQ.setBounds(20,100,100,20);
            frame.getContentPane().add(nrQ);
            nrQueue.setBounds(100,100,50,20);
            frame.getContentPane().add(nrQueue);

            strategy.addItem("Shortest Queue");
            strategy.addItem("Shortest Time");
            strategy.setBounds(20,130,100,20);
            frame.getContentPane().add(strategy);

            start.setBounds(270,160,90,20);
            frame.getContentPane().add(start);
        }

        private void addComponents(){
            simulationTime.setBounds(200,70,100,20);
            frame.getContentPane().add(simulationTime);

            simTime.setBounds(280,70,50,20);
            frame.getContentPane().add(simTime);

            log.setLineWrap(true);
            scr.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            scr.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            log.setBounds(400,10,360,500);
            scr.setBounds(400,15,360,500);
            frame.getContentPane().add(scr);

            time.setBounds(630,520,50,20);
            frame.getContentPane().add(time);

            timeText.setBounds(670,520,50,20);
            frame.getContentPane().add(timeText);
        }


    public JTextField getTimeText() {
        return timeText;
    }

    public void setTimeText(String s) {
        this.timeText.setText(s);
    }

    public String getMaxArriving() {
        return maxArriving.getText();
    }

    public String getMinArriving() {
        return minArriving.getText();
    }

    public String getMaxService() {
        return maxService.getText();
    }

    public String getMinService() {
        return minService.getText();
    }

    public String getNrClienti() {
        return nrClienti.getText();
    }

    public String getNrQueue() {
        return nrQueue.getText();
    }


    public String getSimTime(){
            return simTime.getText();
    }

    public static JTextArea getLog(){
            return log;
    }

    private void butonStart(View v ){
            start.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        String str= (String) strategy.getSelectedItem();
                        Simulation sim = new Simulation(Integer.parseInt(simTime.getText()),Integer.parseInt(maxArriving.getText()),
                                Integer.parseInt(minArriving.getText()),Integer.parseInt(minService.getText()),Integer.parseInt(maxService.getText()),
                                Integer.parseInt(nrClienti.getText()),Integer.parseInt(nrQueue.getText()),v,str);
                        Thread t=new Thread(sim);
                        t.start();

                    }
                    catch (Exception ex){
                        System.out.println(ex.getMessage());
                    }

                }
            });
    }

    public void setPanel(JPanel panel){
            this.panel=panel;
        this.panel.setBackground(Color.decode("#F5DEB3"));
        this.panel.setBounds(10,190,370,360);
        frame.getContentPane().add(this.panel);

    }


    public static void main (String[] args){
        View v= new View();
    }



}
